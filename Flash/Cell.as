﻿package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class Cell extends MovieClip
	{
		public var m_simbol:int;
		public static const NORMAL:int = 1;
		public static const DOWN:int = 2;
		public static const OVER:int = 3;
		public var m_indRow:int;
		public var m_indColumn:int;
		
		public function Cell(indRow:int, indColumn:int)
		{
			m_indRow = indRow;
			m_indColumn = indColumn;
			
			buttonMode = true;
			mouseChildren = false;
			m_simbol = GameEngine.NONE;
			changeImage(NORMAL);
			addEventListener(MouseEvent.MOUSE_DOWN, onDown);
			addEventListener(MouseEvent.MOUSE_OVER, onOver);
			addEventListener(MouseEvent.MOUSE_OUT, onOut);
			addEventListener(MouseEvent.MOUSE_UP, onUp);
		}
		
		private function onDown(event:MouseEvent):void
		{
			changeImage(DOWN);
		}
		
		private function onOver(event:MouseEvent):void
		{
			changeImage(OVER);
		}
		
		private function onOut(event:MouseEvent):void
		{
			changeImage(NORMAL);
		}
		
		private function onUp(event:MouseEvent):void
		{
			if ((m_simbol == GameEngine.NONE && GameEngine.instance.m_whoGoes != GameEngine.NONE && 
				GameEngine.instance.m_gameType == GameEngine.VS_COMPUTER && GameEngine.instance.m_whoGoes == GameEngine.X))
			{
				GameEngine.instance.m_step++;
				
				m_simbol = GameEngine.instance.m_whoGoes;
				
				GameEngine.instance.checkForFinish();
				
				if(GameEngine.instance.m_isFinished)
				{
					Main.instance.initEndRoundStage();
				}
				else
				{
					Main.instance.changeWhoGoes();
					Main.instance.changeWhoGoesLabel();
				}
				
				if(GameEngine.instance.m_gameType == GameEngine.VS_COMPUTER)
				{
					Main.instance.m_timerForComputer = new Timer(300, 1);
					Main.instance.m_timerForComputer.addEventListener(TimerEvent.TIMER_COMPLETE, Main.instance.onTimerCompleteForCompStep);
					Main.instance.m_timerForComputer.start();
				}
			}
			else if((m_simbol == GameEngine.NONE && GameEngine.instance.m_whoGoes != GameEngine.NONE &&
				GameEngine.instance.m_gameType == GameEngine.VS_PLAYER && GameEngine.instance.m_isGameCanStart &&
					GameEngine.instance.m_whoGoes == GameEngine.instance.m_simbolOfPlayer))
			{
				GameEngine.instance.m_step++;
				
				m_simbol = GameEngine.instance.m_whoGoes;
				
				GameEngine.instance.checkForFinish();
				
				if(GameEngine.instance.m_isFinished)
				{
					Main.instance.initEndRoundStage();
				}
				else
				{
					Main.instance.changeWhoGoes();
					Main.instance.changeWhoGoesLabel();
				}
				
				//send increased step, index of the cell, is game finished or not
				Main.instance.m_connection.send("stepMade", this.m_indRow, this.m_indColumn, GameEngine.instance.m_isFinished);
			}
			
			changeImage(OVER);
		}
		
		public function changeImage(imageType:int):void
		{
			switch (imageType)
			{
				case NORMAL: 
					switch (m_simbol)
					{
						case GameEngine.NONE: 
							gotoAndStop("noneNormal");
							break;
						case GameEngine.X: 
							gotoAndStop("xNormal");
							break;
						case GameEngine.O: 
							gotoAndStop("oNormal");
							break;
					}
					break;
				case OVER: 
					switch (m_simbol)
					{
						case GameEngine.NONE: 
							gotoAndStop("noneOver");
							break;
						case GameEngine.X: 
							gotoAndStop("xOver");
							break;
						case GameEngine.O: 
							gotoAndStop("oOver");
							break;
					}
					break;
				case DOWN: 
					switch (m_simbol)
					{
						case GameEngine.NONE: 
							gotoAndStop("noneDown");
							break;
						case GameEngine.X: 
							gotoAndStop("xDown");
							break;
						case GameEngine.O: 
							gotoAndStop("oDown");
							break;
					}
					break;
			}
		}
	}
}
