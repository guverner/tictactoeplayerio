﻿package
{
	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import playerio.*;
	
	public class MenuStage extends MovieClip
	{
		public function MenuStage()
		{
			//change labels for the buttons
			vsPlayerButton.setLabel("VS PLAYER");
			vsComputerButton.setLabel("VS COMP");
			//assign functions for the buttons
			vsPlayerButton.setGoToStageFunction(goToVsPlayerGameStage);
			vsComputerButton.setGoToStageFunction(goToVsComputerGameStage);
		}
		
		private function goToVsComputerGameStage():void
		{
			Main.instance.initGameStage();
			GameEngine.instance.m_gameType = GameEngine.VS_COMPUTER;
		}
		
		private function goToVsPlayerGameStage():void
		{
			PlayerIO.connect(
				stage,								//Referance to stage
				Main.instance.m_gameID,				//Game id (Get your own at playerio.com)
				"public",							//Connection id, default is public
				"guest",							//Username
				"",									//User auth. Can be left blank if authentication is disabled on connection
				null,								//Current PartnerPay partner.
				handleConnect,						//Function executed on successful connect
				handleError							//Function executed if we recive an error
				);
			
			Main.instance.initGameStage();
			GameEngine.instance.m_gameType = GameEngine.VS_PLAYER;
		}
		
		private function handleConnect(client:Client):void
		{
			//trace("Sucessfully connected to player.io");
			
			////Set developmentsever (Comment out to connect to your server online)
			//client.multiplayer.developmentServer = "localhost:8184";
			
			//Create pr join the room test
			client.multiplayer.createJoinRoom(
				"test",								//Room id. If set to null a random roomid is used
				"MyCode",							//The game type started on the server
				true,								//Should the room be visible in the lobby?
				{},									//Room data. This data is returned to lobby list. Variabels can be modifed on the server
				{},									//User join data
				handleJoin,							//Function executed on successful joining of the room
				handleError							//Function executed if we got a join error
			);
		}
		
		private function handleJoin(connection:Connection):void
		{
			//trace("Sucessfully connected to the multiplayer server");
			//gotoAndStop(2);
			 
			Main.instance.m_connection = connection;
			
			//Add disconnect listener
			connection.addDisconnectHandler(handleDisconnect);
			
			//Listen to all messages using a private function
			connection.addMessageHandler("*", handleMessages);
		}
		
		private function handleMessages(m:Message)
		{
			switch(m.type)
			{
				case "playersSign":
					GameEngine.instance.m_simbolOfPlayer = m.getInt(0);
					break;
				case "gameCanStart":
					GameEngine.instance.m_isGameCanStart = true;
					break;
				case "whoGoesFirstStep":
					//this player can go
					GameEngine.instance.m_whoGoesFirstStep = m.getInt(0);
					GameEngine.instance.m_whoGoes = m.getInt(0);
					break;
				case "stepMade":
					GameEngine.instance.m_step++;
					
					Cell(GameEngine.instance.m_board[m.getInt(0)][m.getInt(1)]).m_simbol = GameEngine.instance.m_whoGoes;
					Cell(GameEngine.instance.m_board[m.getInt(0)][m.getInt(1)]).changeImage(Cell.NORMAL);
					
					GameEngine.instance.checkForFinish();
				
					//if(m.getBoolean(2))
					//{
						//Main.instance.initEndRoundStage();
					//}
					//else
					//{
						//Main.instance.changeWhoGoes();
						//Main.instance.changeWhoGoesLabel();
					//}
					
					if(GameEngine.instance.m_isFinished)
					{
						Main.instance.initEndRoundStage();
					}
					else
					{
						Main.instance.changeWhoGoes();
						Main.instance.changeWhoGoesLabel();
					}
					break;
				case "whoGoes":
					GameEngine.instance.m_whoGoes = m.getInt(0);
					Main.instance.changeWhoGoesLabel();
					break;
				case "opponentLeft":
					Main.instance.initMenuStage();
					Main.instance.m_connection.send("userGoMenu");
					break;
			}
		}
		
		private function handleDisconnect():void
		{
			trace("Disconnected from server")
		}
		
		private function handleError(error:PlayerIOError):void
		{
			trace("got", error);
		}
	}
}
