﻿package
{
	import flash.display.SimpleButton;
	import flash.events.MouseEvent;
	
	public class MyButton extends SimpleButton
	{
		private var onUpFinction:Function;
		
		public function MyButton()
		{
			addEventListener(MouseEvent.MOUSE_UP, onUp);
		}
		
		public function onUp(event:MouseEvent):void
		{
			trace("Mouse clicked");
			//removeChild(menuStage);
//			addChild(gameStage);

			onUpFinction();
		}
		
		public function setOnUpFunction(func:Function):void
		{
			onUpFinction = func;
		}
		
		public function setText(str:String):void
		{
			//a.text = str;
			//a.text = "aaa";
		}
	}
}
