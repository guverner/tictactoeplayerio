﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using PlayerIO.GameLibrary;
using System.Drawing;

namespace MyGame {
	public class Player : BasePlayer {
        public int m_sign;
	}

    [RoomType("MyCode")]
	public class GameCode : Game<Player> {

        public const int NONE = 0;
        public const int X = 1;
        public const int O = 2;
        public const int WHO_GOES_FIRST_STEP = X;
        public int m_whoGoes;
        public int m_step;

		// This method is called when an instance of your the game is created
		public override void GameStarted()
        {
            Console.WriteLine("Game is started: " + RoomId);

            m_step = 0;
		}

		// This method is called when the last player leaves the room, and it's closed down.
		public override void GameClosed()
        {
		}

		// This method is called whenever a player joins the game
		public override void UserJoined(Player player)
        {
            if(PlayerCount == 1)
            {
                player.m_sign = X;
                player.Send("playersSign", X);
            }
            else if (PlayerCount == 2)
            {
                player.m_sign = O;
                player.Send("playersSign", O);

                //there is enought players. We can start the game.
                Console.WriteLine("We can start the game!");
                Broadcast("gameCanStart");

                //send who goes first
                m_whoGoes = WHO_GOES_FIRST_STEP;
                Broadcast("whoGoesFirstStep", WHO_GOES_FIRST_STEP);
            }
		}

		// This method is called when a player leaves the game
		public override void UserLeft(Player player)
        {
            foreach (Player guy in Players)
            {
                if (!guy.Equals(player))
                {
                    guy.Send("opponentLeft");
                    break;
                }
            }
		}

        public void changeWhoGoes()
        {
            if (m_whoGoes == X)
            {
                m_whoGoes = O;
            }
            else
            {
                m_whoGoes = X;
            }
        }

		// This method is called when a player sends a message into the server code
		public override void GotMessage(Player player, Message message)
        {
            switch (message.Type)
            {
                case "stepMade":
                    m_step++;

                    foreach (Player guy in Players)
                    {
                        if (!guy.Equals(player))
                        {
                            guy.Send("stepMade", message.GetInt(0), message.GetInt(1), message.GetBoolean(2));
                            break;
                        }
                    }

                    changeWhoGoes();

                    Broadcast("whoGoes", m_whoGoes);
                    break;
                case "userExitRoom":
                    player.Disconnect();
                    break;
                case "userGoMenu":
                    foreach (Player guy in Players)
                    {
                        if (!guy.Equals(player))
                        {
                            guy.Send("opponentLeft");
                            break;
                        }
                    }
                    player.Disconnect();
                    break;
            }
		}
	}
}
